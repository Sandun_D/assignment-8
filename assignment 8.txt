#include <stdio.h>

struct student
{
  char sName[30];
  char subject[15];
  int marks;

};

int main()
{
  int i,sum;
  printf("Enter the number of students (minimum 5 students):");
  scanf("%d",&sum);
  struct student stud[sum];  //Creating an array according to the numbers of students
  
  for(i=1; i <= sum; i++)
    {
      printf("Student %d\n\n",i);  //Getting user inputs

      printf("Enter student name :");
      scanf("%s", stud[i].sName);

      printf("Enter subject name :");
      scanf("%s",stud[i].subject);

      printf("Enter marks :");
      scanf("%d", &stud[i].marks);

      printf("\n");
      
    }
  printf("----------Details of recorded students--------- \n");
  for(i=1; i<= sum; i++)
    {
      printf("Student %d\n\n",i);
      printf("Student Name : %s\n", stud[i].sName);
      printf("Subject : %s\n", stud[i].subject);
      printf("Marks : %d\n", stud[i].marks);
      printf("\n");
    }
  return 0;
}